﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class sceneController : MonoBehaviour
{
    public GameObject objetoVideo;
    public GameObject playButton;

    private bool pointerIn = false;

    public float esperaTiempo;

    void Update()
    {
        contador();
        if (esperaTiempo >= 3)
        {
            reproduceVideo();
            playButton.SetActive(false);
            esperaTiempo = 0;
            pointerIn = false;
        }
    }

    public void reproduceVideo()
    {
        objetoVideo.gameObject.GetComponent<VideoPlayer>().Play();
        objetoVideo.gameObject.GetComponent<AudioSource>().Play();
    }

    public void activaContador()
    {
        pointerIn = true;
    }

    public void desactivaContador()
    {
        pointerIn = false;
    }

    private void contador()
    {
        if (pointerIn)
            esperaTiempo += Time.deltaTime;
        else
            esperaTiempo = 0;
    }    
}
